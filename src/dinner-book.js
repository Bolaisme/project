const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const db = mysql.createConnection({
    host: '192.168.27.19',
    user: 'public',
    password: 'admin',
    database: 'organicfood'
});
db.connect();
const bluebird = require('bluebird');
bluebird.promisifyAll(db);

let per_page = 5;
router.get('/:page?/:keyword?/:type?', (req, res)=>{
    const output = {};
    output.params = req.params;
    output.per_page = per_page;

    let page = parseInt(output.params.page) || 1;
    let keyword = output.params.keyword || '';
    let type = output.params.type || '';

    db.queryAsync(`SELECT COUNT(1) total FROM dinner_list WHERE name LIKE ? OR main_cat = ? `, [`%${keyword}%`, `${type}`])
        .then(results=>{
            output.total = results[0].total;
            output.totalPages = Math.ceil(output.total/per_page);
            if(output.totalPages ==0){
                return;
            }
            if(page<1) page=1;
            if(page>output.totalPages) page = output.totalPages;
            output.page = page;

            return db.queryAsync(`SELECT * FROM dinner_list WHERE name LIKE ? OR main_cat = ? LIMIT ${(page-1)*per_page}, ${per_page}`, [`%${keyword}%`, `${type}`])
        })
            .then(results=>{       
                output.rows = results;

                res.json(output);
            })
            .catch((error)=>{
                res.send('sql: '+error)
            })
});

module.exports = router;