const express = require('express');
const router = express.Router();

// router.get('/admin3/:b1?/:b2?', (req, res)=>{
//     const result = {
//         params: req.params,
//         Url: req.url,
//         baseUrl: req.baseUrl
//     }
//     res.json(result);
// });

// module.exports = router;

router.route('/edit/:name?/:id?/:time?')
    .all((req, res, next)=>{
        res.locals.memberData = {
            name: req.params.name,
            id: req.params.id,
            time: req.params.time
        };
        next();
    })
    .get((req, res) =>{
        res.send('GET: ' + JSON.stringify(res.locals));
    })
    .post((req, res) =>{
        res.send('POST: ' + JSON.stringify(res.locals));
    })

module.exports = router;