const express = require('express');
const app = express();
const url = require('url');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer({dest: 'tmp_uploads/'});
const fs = require('fs');
const session = require('express-session');
const moment = require('moment-timezone')
const mysql = require('mysql');
const db = mysql.createConnection({
    host: '192.168.27.19',
    user: 'public',
    password: 'admin',
    database: 'organicfood'
});
db.connect();

const bluebird = require('bluebird');
bluebird.promisifyAll(db);

const cors = require('cors');
const whitelist = ['http://localhost:8080', 'http://localhost:5000'];
const corsOptions = {
    credentials: true,
    origin: function(origin, callback){
        console.log('origin: ' + origin);
        if(whitelist.indexOf(origin) !== -1 || !origin){
            callback(null, true) //允許
        } else {
            // callback(new Error('Not allowed by CORS'))   //server 會直接停掉
            callback(null, false)  //不允許
        }
    }
};
app.use(cors(corsOptions));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());  // 可以用 JSON 格式做為資料傳遞

app.set('view engine', 'ejs');

app.use(express.static('public'));

// 設定路徑並使用 ejs 樣板
app.get('/', function(request, response){
    response.render('home', {name: 'Paris', img: '/images/paris.jpg'})
})

// app.get('/', function(request, response){
//     response.send('Hello!')
// });

app.get('/abc', function(request, response){
    response.send('Hi!')
});

// 使用 JSON 資料檔 (直接拿)
// app.get('/sales', function(req, res){
//     const data = require(__dirname + '/../data/sales');
//     res.json(data);
// });

// 使用 JSON 資料檔 (使用 template)
app.get('/sales01', function(req, res){
    const data = require(__dirname + '/../data/sales');
    res.render('sales01', {
        my_var: data
    });
});


// 取得 urlencoded parser, 不使用 qs lib, 而使用內建的 querystring lib
const urlencodedParser = bodyParser.urlencoded({extended: false});
app.get('/try-post-form', (req, res)=>{
    res.render('try-post-form');
   
})
// 注意 public 資料夾的路徑應改為絕對路徑
app.get('/try-post-form/123', (req, res)=>{
    res.render('try-post-form');
   
})
// 把 urlencodedParser 當 middleware
app.post('/try-post-form', urlencodedParser, (req, res) => {
    // res.render('try-post-form', req.body);
    console.log(req.body)
    let output = {
        email: req.body.email,
        password: req.body.password
    }
    res.send(output)
})


// fetch 接送資料
app.get('/try-post-form2', (req, res)=>{
    res.render('try-post-form');
   
})
// 把 urlencodedParser 當 middleware
app.post('/try-post-form2', urlencodedParser, (req, res) => {
    // res.render('try-post-form', req.body);
    // console.log(req.body)
    res.json(req.body);
})
// PUT 方法
app.put('/try-post-form2', (req, res)=>{
    res.send("PUT: try-post-form");
});

// multer 上傳多個檔案
app.post('/try-upload', upload.array('photos'), (req, res) => {
    console.log(req.files)
    res.json(req.files);

    req.files.forEach(function(el){   
            switch(el.mimetype){
            case 'image/png':
            case 'image/jpeg':

            fs.createReadStream(el.path)
                .pipe(
                    fs.createWriteStream('public/img/' + el.originalname)
                );
    
                break;
            default:
                return res.send('bad file type');
            }

    })
    
})

// 4.5 ROUTER 處理(使用變數代稱設定路由)
app.get('/my-params1/:member?/:id?', (req, res)=>{
    res.json(req.params);
});
// 4.5.2 使用 regular expression 設定路由
app.get(/^\/09\d{2}\-?\d{3}\-?\d{3}$/, (req, res)=> {
    let str = req.url.slice(1);
    str = str.split('?')[0];
    str = str.split('-').join('');
    res.send('手機:'+str);
})

// 4.5.3 路由模組化(1)
const admin1 = require(__dirname + '/admins/admin1');
admin1(app);

// 4.5.4 路由模組化(2)
const admin2Router = require(__dirname + '/admins/admin2')
app.use(admin2Router);

// 4.5.5 路由模組化(3)
const admin3Router = require(__dirname + '/admins/admin3');
app.use('/dinner', admin3Router);

const dinner_book = require(__dirname + '/dinner-book');
app.use('/dinner-book', dinner_book);

// 5.1 SESSION
app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: '加密用的字串',
    cookie: {
        maxAge: 1200000,
    }
}))

app.get('/try-session', (req, res)=>{
    req.session.my_views = req.session.my_views || 0;
    req.session.my_views ++;
    
    res.json({
        aa: 'hello',
        'my_views': req.session.my_views
    })
})

// 5.2 時間格式
app.get('/try-moment', (req, res)=>{
    const myFormat = 'MMMM Do YYYY, h:mm:ss a';
    const exp = req.session.cookie.expires;
    const mo1 = moment(exp);
    const mo2 = moment(new Date());
    res.contentType('text/plain');
    res.write(exp+"\n");
    res.write('台北 ' + mo1.format(myFormat) + "\n");
    res.write('台北 ' + mo1.tz('Asia/Tokyo').format(myFormat) + "\n");
    res.write('現在時間　' +mo2.format(myFormat)　+ "\n")

    res.end( JSON.stringify(req.session) );
})

// 拿取 database 資料
app.get('/try-qs', (req, res)=>{
    const urlParts = url.parse(req.url, true);
    console.log(urlParts);

    res.render('try-qs', {
        query: urlParts.query
    });
});

// 拿取 dinner_list 資料
// app.get('/dinner', (req, res)=>{
//     var sql = "SELECT * FROM `dinner_list`";
//     db.query(sql, (error, results, fields)=>{
//         console.log(results) 
//         // let results = JSON.stringify(results);
//         if(error) res.send(error);
//         res.send(results);
//         // res.render('dinner', {
//         //     results: results
//         // });
//     });

//     const urlParts = url.parse(req.url, true);
//     console.log(urlParts);  
// });

// 6.4 使用 bluebird 與 hashchange
app.get('/dinner/:page?/:keyword?', (req, res)=>{
    let page = req.params.page || 1;
    let per_page = 5;
    const output = {};
    db.queryAsync("SELECT COUNT(1) total FROM `dinner_list`")
        .then(results=>{
            output.total = results[0].total;
            return db.queryAsync(`SELECT * FROM dinner_list LIMIT ${(page-1)*per_page}, ${per_page}`)
        })
        .then(results=>{
            output.rows = results;
            // res.json(output);
            res.render('dinner', {
                results: output
            })
        })
        .catch((error)=>{
            // console.log('sql error: ', error);
            res.send(error);
        })
})



// 如果上面都沒有, 則顯示錯誤
// app.use((req, res)=>{
//     res.type('html');
//     res.status(404);
//     res.send('頁面錯誤');
// });
app.use((request, response)=>{
    response.render('404page', {img: '/images/404.png'})
})


app.listen(3000, function(){
    console.log('啟動 server 偵聽埠號 3000')
})